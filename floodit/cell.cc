#include "cell.h"
#include "game.h"

/***Cell()
 Purpose:the constructor of the Cell class, initializes all the integer 
 fields to 0 and the game pointer to NULL.
*/
Cell::Cell():
state(0),prevState(0),numNeighbours(0),r(0),c(0),game(NULL){}

/**getState():
Purpose: public getter of the state field
Return : int
*/
int Cell::getState(){
  return state;
}

/**setState():
Purpose: public setter of the state field, sets it to change.
Return : None
*/
void Cell::setState(const int& change){
  prevState=state;
  state=change;
}

/**setCoords():
Purpose: public setter of the r and c fields.
Return : None
*/
void Cell::setCoords(const int r, const int c){
  this->r=r;
  this->c=c;
}

/**setGame():
Purpose: public setter of the game pointer.
Return : None
*/
void Cell::setGame(Game * game){
  this->game=game;
}

/**addNeighbour():
Purpose: increases numNeighbours by one and points at the neighbour cell.
Return : None
*/
void Cell::addNeighbour(Cell *neighbour){
  neighbours[numNeighbours]=neighbour;
  numNeighbours++;
}

/**notifyNeighbours():
Purpose: notifies all the neighbouring cells that the state has changed.
Return : None
*/
void Cell::notifyNeighbours(){
  for(int i=0;i<numNeighbours;i++){
    neighbours[i]->notify(state,prevState);
  }
}

/**notifygame():
Purpose: notifies the game that the state has changed.
Return : None
*/
void Cell:: notifyGame(){
  game->notify(r, c, prevState, state);
}

/**notify(const int & change):
Purpose: changes the state if the state does not equal change and notifies the game and 
the neighbours that the state has changed, otherwise nothing changes
Return : None
*/	
void Cell::notify( const int & change){
  if(state != change){
    prevState=state;
    state=change;
    notifyNeighbours();
    notifyGame();
  }
}

/**notify(const int & current, const int & previous):
Purpose: changes the state if the state equals the previous state and notifies the game and 
the neighbours that the state has changed, otherwise nothing changes
Return : None
*/
void Cell::notify( const int & current, const int & previous){
  if(state == previous){
    prevState=previous;
    state=current;
    notifyNeighbours();
    notifyGame();
  }
}
