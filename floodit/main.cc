#include "controller.h"
#include <iostream>
#include "window.h"
using namespace std;

/*
 * TODO: update main function to take argument "-graphics" for part b. 
 * If -graphics argument is provided, forward this to the controller
 */
int main (int argc, char *argv[]) {
    Controller c;
    if(argc-1==1){
      string theArg = argv[1];
      if(theArg=="-graphics"){
        c.play(true);
      }
      else{
        c.play(false);
      }
    }
    else{
      c.play(false);
    }
}
