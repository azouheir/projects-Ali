#include <iostream>
#include <sstream>
#include "view.h"
#include "textdisplay.h"
using namespace std;

/***TextDisplay(int n)
 Purpose:the constructor of the TextDisplay class
*/
TextDisplay::TextDisplay(int n):View(n){
  theDisplay = new char* [gridSize];
  for(int i=0;i<gridSize;i++){
    theDisplay[i]=new char[gridSize];
    for(int j=0;j<gridSize;j++){
      theDisplay[i][j]='0';
    }
  }
}

/**notify(int r, int c, char ch):
Purpose: changes the state of the theDisplay at (r,c) to ch.
Returns: None
*/
void TextDisplay::notify(int r, int c, char ch){
  theDisplay[r][c]=ch;
}

/**~TextDisplay():
Purpose: destructor for the TextDisplay class.
*/
TextDisplay::~TextDisplay(){
  for(int i=0;i<gridSize;i++){
    delete[]theDisplay[i];
  }
  delete[] theDisplay;
}

/**print(std::ostream &out)
Purpose:prints out all the states in theDisplay to out.
Returns: None
*/
void TextDisplay::print(std::ostream &out){
  for(int i=0;i< gridSize;i++){
    for(int j=0;j< gridSize;j++){
      out<<theDisplay[i][j];
    }
     out<<endl;
  }
}
