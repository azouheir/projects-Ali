#include <iostream>
#include <sstream>
#include "view.h"
#include "graphicdisplay.h"
#include "window.h"

/***GraphicDisplay(int n)
 Purpose:the constructor of the GraphicDisplay class
*/
GraphicDisplay::GraphicDisplay(int n):View(n){
  w=new Xwindow();
  width=500/n;
  height=500/n;
  state = new int* [gridSize];
  for(int i=0;i<gridSize;i++){
    state[i]=new int[gridSize];
    for(int j=0;j<gridSize;j++){
      state[i][j]=0;
    }
  }
}

/**notify(int r, int c, char ch):
Purpose: changes the state at (r,c) to ch.
Returns: None
*/
void GraphicDisplay::notify(int r, int c, char ch){
  int d=ch-'0';
  state[r][c]=d;
}

/**print(std::ostream &out)
Purpose: draws all the states to the Xwindow w.
Returns: None
*/
void GraphicDisplay::print(std::ostream &out){
  int x;
  int y;
  for(int i=0;i<gridSize;i++){
    x=width*i;
    for(int j=0;j<gridSize;j++){
      y=width*j;
      w->fillRectangle(x,y,width,height,state[i][j]);
    }
  }
}

/**~GraphicDisplay():
Purpose: destructor for the GraphicDislay class.
*/
GraphicDisplay::~GraphicDisplay(){
  delete w;
  for(int i=0;i<gridSize;i++){
    delete[]state[i];
  }
  delete[] state;
}
