#include "game.h"
#include "cell.h"
#include "iostream"
using namespace std;


/***Game()
 Purpose:the constructor of the Game class
*/
Game::Game(): gridSize(0),theGrid(NULL),notification(NULL){
  for(int i =0;i <5;i++)colours[i]=0;
}

/**clearGame():
Purpose: frees all the memory from theGrid and sets it to NULL
Returns: None
*/
void Game::clearGame(){
  if(theGrid!=NULL){
    for(int i=0;i<gridSize;i++){
      delete[] theGrid[i];
    }
    delete[] theGrid;
    theGrid=NULL;
  }
}

/**~Game():
Purpose: destructor for the Game class.
*/
Game::~Game(){
  clearGame();
}

/**isWon():
Purpose: returns true if the game is won and false otherwise.
Returns: boolean
*/
bool Game::isWon(){
  int goal=gridSize*gridSize;
  for(int i=0; i<5;i++){
    if(colours[i]==goal) return true;
  }
  return false;
}
void Game::init(int n, GameNotification *gameNotification){
  if(theGrid!=NULL){
    clearGame();
  }
  gridSize=n;
  notification=gameNotification;
  for(int i =0;i <5;i++)colours[i]=0;
  colours[0]=gridSize*gridSize;//white is default
  theGrid=new Cell*[gridSize];
  for(int i=0;i<gridSize;i++){
      theGrid[i]=new Cell[gridSize];
      for(int j=0;j<gridSize;j++){
        theGrid[i][j].setCoords(i,j);
        theGrid[i][j].setGame(this);
      }
  }
  for(int i=0;i<gridSize;i++){
    for(int j=0;j<gridSize;j++){
      if(i!=0){
        theGrid[i][j].addNeighbour(&theGrid[i-1][j]);
      }
      if(j!=0){
        theGrid[i][j].addNeighbour(&theGrid[i][j-1]);
      }
      if(i!=gridSize-1){
        theGrid[i][j].addNeighbour(&theGrid[i+1][j]);
      }
      if(j!=gridSize-1){
        theGrid[i][j].addNeighbour(&theGrid[i][j+1]);
      }
    }
  } 
}

/**change(const int & c)
Purpose: notifies the cell at 0 0 to change it's state to c
Returns: None
*/
void Game::change(const int & c){
  theGrid[0][0].notify(c);
}

/**init(int r, int c, int change)
Purpose: when in init mode it changes the state of the (r,c) cell to change.
Returns: None
*/
void Game::init(int r, int c, int change){
  if(r>=0 && r<gridSize && c>=0 && c<gridSize){
    unsigned int oldState=theGrid[r][c].getState();
    theGrid[r][c].setState(change);
    notify(r,c,oldState,change);//register the change
  }
}

/**notify(int r, int c, unsigned int oldState, unsigned int newState)
Purpose: notifies the notification class that the state at (r,c) has changed.
Returns: None
*/
void Game::notify(int r, int c, unsigned int oldState, unsigned int newState){
    colours[newState]++;
    colours[oldState]--;
    notification->notify(r,c,newState);//
}
